#include "GameState.hpp"
#include <vector>
#include <iostream>
#include <ctime>

using namespace std;

GameState::GameState(const Hand &myHand, const vector<Card> &game, const Packet &playedThisTurn){
	_hands.push_back(myHand);//SOUTH
	_hands.push_back(Hand(WEST));
	_hands.push_back(Hand(NORTH));
	_hands.push_back(Hand(EAST));

	_pliNumber=0;
	Packet pli=playedThisTurn;
	_pli.push_back(pli);

	generateHands(game);
	
	_playerOrder.push_back(SOUTH);
}

void GameState::generateHands(vector<Card> game)
{
	//permet de savoir le nombre de carte dans le jeu de chaque adversaire (compliqué à simplifier)
	int nbCards[3];//stock le nombre de cartes de chacun des autres joueurs
	
	int nbcardPlayed = _pli[0].size();
	int nbCardmyHand = _hands[0].size();
	
	if(nbcardPlayed == 1){
		nbCards[0] = nbCards[1] = nbCardmyHand;
		nbCards[2] = nbCardmyHand -1;
	}
	else if(nbcardPlayed == 2)
	{
		nbCards[0] =  nbCardmyHand;
		nbCards[2] = nbCards[1] = nbCardmyHand -1;
	}else if(nbcardPlayed == 3){
		nbCards[2] = nbCards[1] = nbCards[0] = nbCardmyHand -1;
	}
	else{
		nbCards[0] = nbCards[1] = nbCards[2] = nbCardmyHand;
	}

	std::srand ( unsigned ( std::time(0) ) );
		
	std::random_shuffle(game.begin(), game.end());
	
	int handToFill = 0;
	for(unsigned int i=0; i<game.size(); i++){
		_hands[handToFill+1].pushCard(game[i]);
		if(_hands[handToFill+1].size()==nbCards[handToFill])
		   handToFill++;
	}

	for(unsigned int i=0; i<4; i++){
		_hands[i].sort();
	}
}

void GameState::authorized(vector<int> &authorized) const{
	//authorizedToPlay(_hands[_playerOrder.back()], _pli[_pliNumber], authorized);
	authorized.clear();
	//print();
	_hands[_playerOrder.back()].authorized(_pli[_pliNumber], authorized);
}

int GameState::play(int i, bool &trickEnd, bool &friendWin){
	trickEnd=false;
	friendWin=false;
	_pli[_pliNumber].pushCard(_hands[_playerOrder.back()][i], _playerOrder.back());
	_hands[_playerOrder.back()].erase(i);
	
	//test fin du pli et détermination du prochain joueur
	if(_pli[_pliNumber].size() == 4){
		Player nextPlayer=_pli[_pliNumber].winPlayer();
		//int score = trickScore(_pli[_pliNumber], nextPlayer);
		int score = _pli[_pliNumber].trickScore();
		_playerOrder.push_back(nextPlayer);
		trickEnd=true;
		if(nextPlayer==NORTH || nextPlayer==SOUTH)
			friendWin=true;
		else{
			friendWin=false;
			score=-score;
		}
		
		_pliNumber++;
		Packet temp;
		_pli.push_back(temp);//on ajoute une dimension dans le vecteur
		
		return score;
	}
	else
	{
		_playerOrder.push_back((Player)((_playerOrder.back()+1)%4));
		trickEnd=false;
		return 0;	
	}
}

void GameState::unplay(){

	if(_pli[_pliNumber].size()==0){
		_pli.pop_back();
		_pliNumber--;
	}
	Player player;
	Card card=_pli[_pliNumber].back(player);
	_pli[_pliNumber].popCard();
	_hands[player].pushCard(card);
	_hands[player].sort();
	_playerOrder.pop_back();
}

void GameState::print() const{
	for(int i=0; i<8; i++){
		for(int j=0; j<4; j++){
			if(i<_hands[j].size())
				cout << (Card)_hands[j][i];
			cout << "\t";
		}
		cout << std::endl;
	}

	//cout << std::endl;


	for(int i=0; i<_pli[_pliNumber].size(); i++){
		cout << "\t    ";
		cout << _pli[_pliNumber][i] << std::endl;
	}

	cout << std::endl<<std::endl;
}
