#ifndef _GAMETREE_HPP
#define _GAMETREE_HPP

#include "Card.hpp"
#include "Packet.hpp"
#include <vector>
#include <algorithm>



using namespace std;

class GameState{
	public:

	//game ne contient pas les cartes jouées ce tour ci
	GameState(const Hand &myHand, const vector<Card> &game, const Packet &playedThisTurn);//construit le premier jeu
	
	void generateHands(vector<Card> game);//remplit aléatoirement les mains des autres joueurs
	
	void authorized(vector<int> &authorized) const;

	//joue la carte de id i renvoi le score, trick end ?? friendWin indique si c'est le camp allié qui gagne
	int play(int i, bool &trickEnd, bool &friendWin);
	void unplay();
	
	void print() const;//affiche les mains, et le paquet de l'état

private:
	//tableau statique contenant les 4 jeux initiaux de 8 cartes...
	//vector<Card> _hands[4];
	vector<Hand> _hands;
	//vector<vector<Card> > _pli;//empile les différents plis traité dans l'arbre
	vector<Packet> _pli;
	int _pliNumber;//stock le numéro du pli actuel
	
	vector<Player> _playerOrder;
	
	int trickScore(const Packet &packet, Player &nextPlayer) const;

	//renvoi un vecteur de int indiquant que l'on peut la carte d'indice i
	//void authorizedToPlay(const Hand &hand, const Packet &cardPlayed, vector<int> &authorized) const;

	//bool pliPerdant(const Packet playedThisTurn) const;
	
	//player contient le dernier joueur ayant joué et contiendra en sortie de la fonction le joueur qui commencera le prochain pli
	//A REVOIR PAS FINI
	


	
};

#endif
