#include "Packet.hpp"

#include <stdlib.h>
#include <algorithm>

using namespace std;

int Packet::trickScore() const {
	int score = 0;

	//test size pack
	for (unsigned int i = 0; i < _pack.size(); i++) {
		score += _pack[i].points();
	}

	return score;
}

Player Packet::winPlayer() const {
	int point, maxPoint = 0;
	bool ifAssets = false;
	Card winCard=_pack[0];
	Card::Colour askColour = winCard.colour();
	int posMax=0;

	Player player = _player[0];

	for (unsigned int i = 1; i < _pack.size(); i++) {
		if (_pack[i].isAssets()) {
			if (ifAssets) {
				if (winCard.value() < _pack[i].value()) {
					winCard = _pack[i];
					posMax = i;
					player = _player[i];
				}
			}else{
				ifAssets=true;
				winCard = _pack[i];
				posMax = i;
				player = _player[i];
			}
		}
		else if(_pack[i].colour()==askColour){
			if(!ifAssets){
				if(winCard.value()<_pack[i].value()){
					posMax=i;
					winCard=_pack[i];
					player = _player[i];
				}
			}
		}
	}
	return player;
}
bool Packet::loose() const {
	if(_pack.size()==0){
		return false;
	}
	if(_pack.size()==1){
			return true;
	}
	if(_pack.size()>3){
		//erreur
		exit(-1);
	}
	Player player = (Player)((_player.back()+1)%4);
	Player win = winPlayer();

	if(player == win || player == (win+2)%4){
		return false;
	}else
		return true;
}

void Packet::pushCard(Card card, Player player) {
	_pack.push_back(card);
	_player.push_back(player);
}
void Packet::popCard() {
	_pack.pop_back();
	_player.pop_back();
}


//à vérifier
int Packet::maxAssets() const{
	int val=-1;
	bool ifAssets = false;
	for(unsigned int i=0; i<_pack.size(); i++){
		if(_pack[i].isAssets()){
			if(!ifAssets){
				ifAssets=true;
				val=_pack[i].power();
			}else{
				if(val<_pack[i].power())
					val=_pack[i].power();
			}
		}
	}

	return val;
}

Card Packet::operator[](unsigned int i) const {
	return _pack[i];
}

//reflechir à un constructeur pratique
Hand::Hand(Player player) :_player(player), _hand() {
}

void Hand::pushCard(Card card) {
	_hand.push_back(card);
}

//à tester et verifier
void Hand::authorized(const Packet &pack, vector<int> &idAuthorized) const {
	bool haveToPlayAssets = false;
	int maxValAssets;
	if (pack.size() == 0) {
		//authorized.assign(hand.size(), true);
		for (unsigned int i = 0; i < _hand.size(); i++) {
			idAuthorized.push_back(i);
		}
		return;
	}

	if (!pack[0].isAssets()) { //pas de l'atout
		//on regarde si on la couleur de la première carte
		bool ifColour = false;
		for (unsigned int i = 0; i < _hand.size(); i++) {
			if (pack[0].colour() == _hand[i].colour()) {
				idAuthorized.push_back(i);
				ifColour = true;
			}
		}
		if (ifColour) {		//on a de la couleur
			return;
		}
		if (!pack.loose()) {		//pli gagnant on joue ce qu'on veux
			for (unsigned int i = 0; i < _hand.size(); i++) {
				idAuthorized.push_back(i);
			}

			return;
		} else
			haveToPlayAssets = true;
	} else
		haveToPlayAssets = true;
	if (haveToPlayAssets) {	//on regarde si on a de l'atout si c'est le cas on essaie de monter sinon on joue ce qu'on veut
		bool hasAssets = false;
		bool canUp = false;
		maxValAssets=pack.maxAssets();
		for (unsigned int i = 0; i < _hand.size(); i++) {
			if (_hand[i].isAssets()) {
				if(hasAssets==false){
					hasAssets = true;

				}
				if (_hand[i].power() > maxValAssets) {
					canUp = true;
					idAuthorized.push_back(i);
				}
			}
		}
		if (hasAssets) {
			if (canUp)
				return;
			else {
				for (unsigned int i = 0; i < _hand.size(); i++) {
					if (_hand[i].isAssets()) {
						idAuthorized.push_back(i);
					}
				}
			}
		} else {
			for (unsigned int i = 0; i < _hand.size(); i++) {
				idAuthorized.push_back(i);
			}
			return;
		}
	}
}

void Hand::erase(unsigned int i) {
	_hand.erase(_hand.begin() + i);
}

void Hand::play(unsigned int i, Packet &pack) {
	pack.pushCard(_hand[i], _player);
	erase(i);
}

Card Hand::operator[](unsigned int i) const {
	return _hand[i];
}
void Hand::sort(){
	std::sort(_hand.begin(), _hand.end());
}
