#include "Card.hpp"
bool operator==(const Card &c1, const Card &c2){
	if(c1.value() == c2.value() && c1.colour()== c2.colour())
		return true;
	else
		return false;
}

bool operator!=(const Card &c1, const Card &c2){
	if(c1.value() != c2.value() || c1.colour() != c2.colour())
		return true;
	else
		return false;
}

bool operator<(const Card &c1, const Card &c2){
	if(c1.colour() < c2.colour())
		return true;
	else if(c1.colour() > c2.colour())
		return false;
	else{
		if(c1.value() < c2.value())
			return true;
		else
			return false;
	}
}

std::ostream& operator << (std::ostream& O, const Card& c){
	switch(c.colour()){
	case Card::Clubs:
		O << CLUBS;
		break;
	case Card::Spades:
			O << SPADES;
			break;
	case Card::Diamonds:
			O << DIAMONDS;
			break;
	case Card::Hearts:
			O << HEARTS;
			break;
	};

	if((int)c.value()<11)
		O << (int)c.value();
	else{
		switch(c.value()){

			case Card::Jack:
					O << "J";
					break;
			case Card::Queen:
					O << "Q";
					break;
			case Card::King:
					O << "K";
					break;
			case Card::Ace:
					O << "A";
					break;
			};
	}

	return O;
}

Card::Card(Card::Colour colour, Card::Value value): _colour(colour), _value(value){
}
Card::Card(): _colour(None), _value(Null){
}

bool Card::isAssets() const{
	if(_colour == _colourAssets)
		return true;
	else
		return false;
}

inline Card::Colour Card::colour() const{
	return _colour;
}

inline Card::Value Card::value() const{
	return _value;
}

int Card::power() const{
	int point = points();
	if(point==0){
		switch(_value){
		case Seven:
			point=-1;
			break;
		case Eight:
			point=0;
			break;
		case Nine:
			point=1;
			break;
		default:
			//il y a un problème
			//exit(-1);
			break;
		};
	}
	return point;
}

int Card::points() const{
	int points;
	switch(_value){
		case Seven:
			points=0;
			break;
		case Eight:
			points=0;
			break;
		case Nine:
			if(this->isAssets())
				points=14;
			else
				points=0;
			break;
		case Ten:
			points=10;
			break;
		case Jack:
			if(this->isAssets())
				points=20;
			else
				points=2;
			break;
		case Queen:
			points=3;
			break;
		case King:
			points=4;
			break;
		case Ace:
			points=11;
			break;
	}

	return points;
}



