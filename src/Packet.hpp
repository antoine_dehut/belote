#ifndef _PACKET_HPP
#define _PACKET_HPP

#include "Card.hpp"
#include <vector>

class Packet{
private:
	std::vector<Card> _pack;
	std::vector<Player> _player;

public:
	//Packet();
	int trickScore() const;
	void pushCard(Card card, Player player);
	void popCard();

	Player winPlayer() const;
	bool loose() const;
	int maxAssets() const;

	int size() const{
		return _pack.size();
	}
	Card operator[] (unsigned int i) const;
	Card back(Player &player){
	player=_player.back();
		return _pack.back();
	}
};
class Hand{
private:
	std::vector<Card> _hand;
	Player _player;

public:
	Hand(Player player);//faire un constructeur pratique pour remplir la main d'un coup
	void authorized(const Packet &pack, std::vector<int> &idAuthorized) const;
	Card operator[] (unsigned int i) const;
	void erase(unsigned int i);
	void play(unsigned int i, Packet &pack);
	void pushCard(Card card);

	int size() const{
		return _hand.size();
	}

	void sort();
};

#endif
