#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "GameState.hpp"

#define INFINI 1000
#define M_INFINI -1000

using namespace std;

class AlphaBeta
{
private:
	/*int _evaluatedStates;
	bool _replayBestMoves;*/
	int _idBestCard;

	ofstream fichierAlphaBeta;
	void maxBestCard(int &scoreActuel, int tempScore, int &imax, int i);

protected:

public:

	AlphaBeta();
	//AlphaBeta();

	~AlphaBeta();

	int alphaBetaMax(GameState &game, int alpha, int beta, int nbTrick, int score, bool firstCall=false);
	int alphaBetaMin(GameState &game, int alpha, int beta, int nbTrick, int score);
	int minMaxAlphaBeta(GameState game, int nbTrick);


};
