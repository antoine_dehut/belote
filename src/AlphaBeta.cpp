﻿#include "AlphaBeta.hpp"

AlphaBeta::AlphaBeta() : _idBestCard(0)
{
	fichierAlphaBeta.open("fichierAlphaBeta.txt", ios::out | ios::trunc);
	
    if(fichierAlphaBeta)
    {
		cout << "Fichier ouvert !" << endl;
    }
    else
		cerr << "Erreur à l'ouverture !" << endl;

	//minMaxAlphaBeta(game, M_INFINI, INFINI, nbTrick, 0);

}
AlphaBeta::~AlphaBeta(){
	fichierAlphaBeta.close();
}

int AlphaBeta::alphaBetaMax(GameState &game, int alpha, int beta, int nbTrick, int score, bool firstCall)
{
	int score_final=M_INFINI, tmp, nbMoves=0;
	bool trickEnd, friendWin;
	std::vector<int> possibleMoves;

	fichierAlphaBeta << "Max " << "Alpha: " << alpha << " " << "Beta: " << beta << " " << "Pli restant: " << nbTrick << " " << "Score: " << score << endl;
	
	if (nbTrick==0){
		fichierAlphaBeta << "feuille max : score = " << score << endl;
	   return score;
	}

	game.authorized(possibleMoves);
	nbMoves=possibleMoves.size();

	for (int i=0; i<nbMoves; i++)
	{
		//game.print();
		tmp=game.play(possibleMoves[i], trickEnd, friendWin);
		game.print();

		if(trickEnd)
		{
			score +=tmp;
			if(friendWin){
				if(firstCall){
					int tempScore = alphaBetaMax(game, alpha, beta, nbTrick-1, score);
					maxBestCard(score_final, tempScore, _idBestCard, possibleMoves[i]);
				}else
					score_final = max(score_final, alphaBetaMax(game, alpha, beta, nbTrick-1, score));
			}
			else{
				if(firstCall){
					int tempScore = alphaBetaMin(game, alpha, beta, nbTrick-1, score);
					maxBestCard(score_final, tempScore, _idBestCard, possibleMoves[i]);
				}else
					score_final = max(score_final, alphaBetaMin(game, alpha, beta, nbTrick-1, score));
			}
		}
		else{
			if(firstCall){
					int tempScore = alphaBetaMin(game, alpha, beta, nbTrick, score);
					maxBestCard(score_final, tempScore, _idBestCard, possibleMoves[i]);
				}else
					score_final = max(score_final, alphaBetaMin(game, alpha, beta, nbTrick, score));
		}
		
		game.unplay();

		if (score_final>=beta)
			return score_final;
		if (score_final>alpha)
			alpha=score_final;
	}
	return score_final;
}
 
int AlphaBeta::alphaBetaMin(GameState &game, int alpha, int beta, int nbTrick, int score)
{
	int score_final=INFINI, tmp, nbMoves=0;
	bool trickEnd, friendWin;
	std::vector<int> possibleMoves;
	
	fichierAlphaBeta << "Min " << "Alpha: " << alpha << " " << "Beta: " << beta << " " << "Pli restant: " << nbTrick << " " << "Score: " << score << endl;

	if (nbTrick==0){
		fichierAlphaBeta << "feuille min : score = " << score << endl;
	   return score;
	}

	game.authorized(possibleMoves);
	nbMoves=possibleMoves.size();

	for (int i=0; i<nbMoves; i++)
	{
		//game.print();
		tmp=game.play(possibleMoves[i], trickEnd, friendWin);
		game.print();

		if(trickEnd)
		{
			score+=tmp;
			if(friendWin)
				score_final = min(score_final, alphaBetaMax(game, alpha, beta, nbTrick-1, score));
			else
				score_final = min(score_final, alphaBetaMin(game, alpha, beta, nbTrick-1, score));
		}
		else
			score_final = min(score_final, alphaBetaMax(game, alpha, beta, nbTrick, score));

		game.unplay();

		if (score_final<=alpha)
			return score_final;
		if (score_final<beta)
			beta=score_final;
	}
	return score_final;
}

int AlphaBeta::minMaxAlphaBeta(GameState game, int nbTrick){
	int best;
	best=alphaBetaMax(game, M_INFINI, INFINI, nbTrick, 0 ,true);

	return best;
}

void AlphaBeta::maxBestCard(int &scoreActuel, int tempScore, int &imax, int i){
	if(scoreActuel < tempScore){
		scoreActuel=tempScore;
		imax=i;
	}
}
