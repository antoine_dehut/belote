#ifndef _CARD_HPP
#define _CARD_HPP
#include <iostream>

#define HEARTS "\xe2\x99\xa5"
#define CLUBS "\xe2\x99\xa3"
#define DIAMONDS "\xe2\x99\xa6"
#define SPADES "\xe2\x99\xa0"

enum Player{SOUTH=0, WEST=1, NORTH=2, EAST=3};

class Card{
public:
	enum Colour {None=-1, Spades=0, Hearts=1, Clubs=2, Diamonds=3};
	enum Value {Null=-1, Seven=7, Eight=8, Nine=9, Ten=10, Jack=11, Queen=12, King=13 , Ace=14};
	
	Card(Colour colour, Value value);
	Card();
	
	bool isAssets() const;
	inline Colour colour() const;
	inline Value value() const;
	int points() const;
	int power() const;
	
	static Colour _colourAssets;//ptetre a revoir
private:
	Colour _colour;
	Value _value;
};

bool operator==(const Card &c1, const Card &c2);
bool operator!=(const Card &c1, const Card &c2);
bool operator<(const Card &c1, const Card &c2);
std::ostream& operator << (std::ostream& O, const Card& c);
#endif
