



#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE testIaBelote
//#include <boost/test/included/unit_test.hpp>
#include <boost/test/unit_test.hpp>

#define private public

#include "GameState.hpp"
#include "Card.hpp"
#include "Packet.hpp"
#include "AlphaBeta.hpp"

#include <vector>
//#include <algorithm>


using namespace std;

Card::Colour Card::_colourAssets;

struct SetUpCard{
	Card _card;
	vector<Card> _game;

	SetUpCard(): _card(Card::Spades, Card::Seven){
		Card::_colourAssets = Card::Spades;
	}
};

BOOST_FIXTURE_TEST_SUITE(card, SetUpCard)
BOOST_AUTO_TEST_CASE(testConstructeur)
{
	BOOST_CHECK_EQUAL(_card.colour(), Card::Spades);
	BOOST_CHECK_EQUAL(_card.value(), Card::Seven);
}

BOOST_AUTO_TEST_CASE(testIsAssets){
	BOOST_CHECK(_card.isAssets());
}

BOOST_AUTO_TEST_CASE(testPoints){
	BOOST_CHECK_EQUAL(_card.points(), 0);
	Card card1(Card::Spades, Card::Jack);
	BOOST_CHECK_EQUAL(card1.points(), 20);
	Card card2(Card::Hearts, Card::Jack);
	BOOST_CHECK_EQUAL(card2.points(), 2);
}
BOOST_AUTO_TEST_SUITE_END()


struct SetUpPacket{
	Card _card;
	Packet _pack1;
	Packet _pack2;
	Packet _pack3;

	SetUpPacket(): _card(Card::Spades, Card::Seven){
		Card::_colourAssets = Card::Spades;

		_pack1.pushCard(Card(Card::Hearts, Card::Eight), NORTH);
		_pack1.pushCard(Card(Card::Hearts, Card::Seven), EAST);
		_pack1.pushCard(Card(Card::Spades, Card::Jack), SOUTH);
		_pack1.pushCard(Card(Card::Clubs, Card::King), WEST);

		_pack2.pushCard(Card(Card::Hearts, Card::Eight), EAST);
		_pack2.pushCard(Card(Card::Clubs, Card::Seven), NORTH);
		_pack2.pushCard(Card(Card::Hearts, Card::King), WEST);
		_pack2.pushCard(Card(Card::Diamonds, Card::King), SOUTH);

		_pack3.pushCard(Card(Card::Hearts, Card::Eight), NORTH);
		_pack3.pushCard(Card(Card::Clubs, Card::Seven), EAST);
		_pack3.pushCard(Card(Card::Clubs, Card::Queen), SOUTH);
		_pack3.pushCard(Card(Card::Diamonds, Card::King), WEST);

	}
};
/*
BOOST_FIXTURE_TEST_SUITE(packet, SetUpPacket)
BOOST_AUTO_TEST_CASE(testWinPlayer){
	BOOST_CHECK_EQUAL(_pack1.winPlayer(), SOUTH);
	BOOST_CHECK_EQUAL(_pack2.winPlayer(), WEST);
	BOOST_CHECK_EQUAL(_pack3.winPlayer(), NORTH);
}
BOOST_AUTO_TEST_CASE(testTrickScore){
	BOOST_CHECK_EQUAL(_pack1.trickScore(), 24);
	BOOST_CHECK_EQUAL(_pack2.trickScore(), 8);
	BOOST_CHECK_EQUAL(_pack3.trickScore(), 7);
}
BOOST_AUTO_TEST_CASE(testPopCard){
	_pack1.popCard();
	BOOST_CHECK_EQUAL(_pack1.size(), 3);
	BOOST_CHECK_EQUAL(_pack1[0].value(), Card::Eight);
	BOOST_CHECK_EQUAL(_pack1[1].colour(), Card::Hearts);
	BOOST_CHECK_EQUAL(_pack1[2].value(), Card::Jack);
}
BOOST_AUTO_TEST_CASE(testLoose){
	_pack1.popCard();
	BOOST_CHECK_EQUAL(_pack1.loose(), true);
	_pack1.popCard();
	BOOST_CHECK_EQUAL(_pack1.loose(), false);
	_pack1.popCard();
	BOOST_CHECK_EQUAL(_pack1.loose(), true);
	_pack1.popCard();
	BOOST_CHECK_EQUAL(_pack1.loose(), false);

	_pack2.popCard();
	BOOST_CHECK_EQUAL(_pack2.loose(), true);
	_pack2.popCard();
	BOOST_CHECK_EQUAL(_pack2.loose(), false);
	_pack2.popCard();
	BOOST_CHECK_EQUAL(_pack2.loose(), true);
	_pack2.popCard();
	BOOST_CHECK_EQUAL(_pack2.loose(), false);
}

BOOST_AUTO_TEST_CASE(testBack){
	BOOST_CHECK_EQUAL(_pack1.back().value(), Card::King);
	BOOST_CHECK_EQUAL(_pack1.back().colour(), Card::Clubs);
}
BOOST_AUTO_TEST_SUITE_END()
*/
struct SetUpHand{
	Packet _pack1, _pack2;
	Hand _hand;
	SetUpHand(): _hand(SOUTH){
		Card::_colourAssets = Card::Spades;

		_pack1.pushCard(Card(Card::Hearts, Card::Eight), NORTH);
		_pack1.pushCard(Card(Card::Hearts, Card::Seven), EAST);

		_pack2.pushCard(Card(Card::Diamonds, Card::Eight), NORTH);
		_pack2.pushCard(Card(Card::Diamonds, Card::King), EAST);

		_hand.pushCard(Card(Card::Hearts, Card::Nine));
		_hand.pushCard(Card(Card::Hearts, Card::Ace));
		_hand.pushCard(Card(Card::Spades, Card::Jack));
		_hand.pushCard(Card(Card::Clubs, Card::King));
		_hand.pushCard(Card(Card::Clubs, Card::Ten));
		_hand.pushCard(Card(Card::Spades, Card::Seven));
	}
};
BOOST_FIXTURE_TEST_SUITE(hand, SetUpHand)
BOOST_AUTO_TEST_CASE(testAuthorized){
	vector<int> id;

	vector<int> attemptedID;
	attemptedID.push_back(0);
	attemptedID.push_back(1);


	_hand.authorized(_pack1, id);
	BOOST_CHECK_EQUAL(id.size(), 2);
	BOOST_CHECK_EQUAL_COLLECTIONS(id.begin(), id.end(), attemptedID.begin(), attemptedID.end());
	id.clear();
	attemptedID.clear();
	attemptedID.push_back(2);
	attemptedID.push_back(5);
	_hand.authorized(_pack2, id);
	BOOST_CHECK_EQUAL(id.size(), 2);
	BOOST_CHECK_EQUAL_COLLECTIONS(id.begin(), id.end(), attemptedID.begin(), attemptedID.end());
}
BOOST_AUTO_TEST_CASE(testErase){
	_hand.erase(2);
	Card card(Card::Clubs, Card::King);
	BOOST_CHECK_EQUAL(_hand[2].value(), card.value());
	BOOST_CHECK_EQUAL(_hand[2].colour(), card.colour());
	BOOST_CHECK_EQUAL(_hand.size(), 5);

}
BOOST_AUTO_TEST_SUITE_END()

struct SetUpGameState{
	Hand _myHand;
	Packet _pack1;
	vector<Card> _game;
	GameState *_state;

	void fakeGenerateHands(GameState *state){
		Hand hand1(SOUTH), hand2(WEST), hand3(NORTH), hand4(EAST);
		Packet pack;

		state->_pliNumber=0;


		hand1.pushCard(Card(Card::Spades, Card::Nine));
		hand1.pushCard(Card(Card::Hearts, Card::Seven));
		hand1.pushCard(Card(Card::Hearts, Card::Ten));
		hand1.pushCard(Card(Card::Clubs, Card::Seven));
		hand1.pushCard(Card(Card::Clubs, Card::Eight));
		hand1.pushCard(Card(Card::Clubs, Card::King));
		hand1.pushCard(Card(Card::Diamonds, Card::Ace));
		hand1.pushCard(Card(Card::Diamonds, Card::Ten));
		state->_hands[0]=hand1;


		hand2.pushCard(Card(Card::Spades, Card::Seven));
		hand2.pushCard(Card(Card::Spades, Card::Ten));
		hand2.pushCard(Card(Card::Spades, Card::King));
		hand2.pushCard(Card(Card::Hearts, Card::Jack));
		hand2.pushCard(Card(Card::Diamonds, Card::Nine));
		hand2.pushCard(Card(Card::Diamonds, Card::Eight));
		hand2.pushCard(Card(Card::Clubs, Card::Nine));
		hand2.pushCard(Card(Card::Diamonds, Card::Jack));

		state->_hands[1]=hand2;

		hand3.pushCard(Card(Card::Spades, Card::Queen));
		hand3.pushCard(Card(Card::Spades, Card::Eight));
		hand3.pushCard(Card(Card::Diamonds, Card::Seven));
		hand3.pushCard(Card(Card::Clubs, Card::Queen));
		hand3.pushCard(Card(Card::Clubs, Card::Jack));
		hand3.pushCard(Card(Card::Spades, Card::Jack));
		hand3.pushCard(Card(Card::Spades, Card::Ace));
		state->_hands[2]=hand3;

		hand4.pushCard(Card(Card::Clubs, Card::Ten));
		hand4.pushCard(Card(Card::Diamonds, Card::King));
		hand4.pushCard(Card(Card::Clubs, Card::Ace));
		hand4.pushCard(Card(Card::Hearts, Card::Ace));
		hand4.pushCard(Card(Card::Hearts, Card::Queen));
		hand4.pushCard(Card(Card::Hearts, Card::Eight));
		hand4.pushCard(Card(Card::Diamonds, Card::Queen));
		state->_hands[3]=hand4;



		pack.pushCard(Card(Card::Hearts, Card::King), NORTH);
		pack.pushCard(Card(Card::Hearts, Card::Nine), EAST);
		state->_pli.clear();
		state->_pli.push_back(pack);

		for(unsigned int i=0; i<4; i++){
			state->_hands[i].sort();
		}
	}

	void suppHandPacket(vector<Card> &game, const Hand &hand, const Packet &pack){
		for(int i=0; i<hand.size(); i++){
			game.erase(std::remove(game.begin(), game.end(), hand[i]), game.end());
		}
		for(int i=0; i<pack.size(); i++){
			game.erase(std::remove(game.begin(), game.end(), pack[i]), game.end());
		}
	}

	SetUpGameState(): _myHand(SOUTH){
		_myHand.pushCard(Card(Card::Hearts, Card::Seven));
		_myHand.pushCard(Card(Card::Spades, Card::Nine));
		_myHand.pushCard(Card(Card::Diamonds, Card::Ace));
		_myHand.pushCard(Card(Card::Clubs, Card::King));
		_myHand.pushCard(Card(Card::Clubs, Card::Seven));
		_myHand.pushCard(Card(Card::Hearts, Card::Ten));
		_myHand.pushCard(Card(Card::Clubs, Card::Eight));
		_myHand.pushCard(Card(Card::Diamonds, Card::Ten));

		_pack1.pushCard(Card(Card::Hearts, Card::King), NORTH);
		_pack1.pushCard(Card(Card::Hearts, Card::Nine), EAST);

		//on génère un jeu complet
		for(int i=0; i<4; i++){
			for(int j=0+7; j<8+7; j++){
				_game.push_back(Card((Card::Colour)i, (Card::Value)j));
			}
		}

		suppHandPacket(_game, _myHand, _pack1);
		_state = new GameState(_myHand, _game, _pack1);

	}

};
BOOST_FIXTURE_TEST_SUITE(gamestate, SetUpGameState)
/*BOOST_AUTO_TEST_CASE(testConstructor){

}*/
BOOST_AUTO_TEST_CASE(testGenerateHands){

	BOOST_CHECK_EQUAL(_state->_hands[0].size(), 8);
	BOOST_CHECK_EQUAL(_state->_hands[1].size(), 8);
	BOOST_CHECK_EQUAL(_state->_hands[2].size(), 7);
	BOOST_CHECK_EQUAL(_state->_hands[3].size(), 7);

	//_state->print();

}
BOOST_AUTO_TEST_CASE(testAuthorized){
	vector<int> author;
	vector<int> attempted_vec;

	attempted_vec.push_back(1);
	attempted_vec.push_back(2);


	_state->authorized(author);

	BOOST_CHECK_EQUAL_COLLECTIONS(author.begin(), author.end(), attempted_vec.begin(), attempted_vec.end());

}
BOOST_AUTO_TEST_CASE(testsimplePlay){
	bool trickend, friendwin;
	_state->play(1, trickend, friendwin);
	Hand attempted_hand(SOUTH);
	Packet attempted_pack;

	attempted_hand.pushCard(Card(Card::Spades, Card::Nine));
	attempted_hand.pushCard(Card(Card::Diamonds, Card::Ace));
	attempted_hand.pushCard(Card(Card::Clubs, Card::King));
	attempted_hand.pushCard(Card(Card::Clubs, Card::Seven));
	attempted_hand.pushCard(Card(Card::Hearts, Card::Ten));
	attempted_hand.pushCard(Card(Card::Clubs, Card::Eight));
	attempted_hand.pushCard(Card(Card::Diamonds, Card::Ten));

	attempted_hand.sort();

	attempted_pack.pushCard(Card(Card::Hearts, Card::King), NORTH);
	attempted_pack.pushCard(Card(Card::Hearts, Card::Nine), EAST);
	attempted_pack.pushCard(Card(Card::Hearts, Card::Seven), SOUTH);

	BOOST_CHECK_EQUAL_COLLECTIONS(_state->_hands[0]._hand.begin(), _state->_hands[0]._hand.end(), attempted_hand._hand.begin(), attempted_hand._hand.end());
	BOOST_CHECK_EQUAL_COLLECTIONS(_state->_pli[0]._pack.begin(), _state->_pli[0]._pack.end(), attempted_pack._pack.begin(), attempted_pack._pack.end());

	//_state->print();

}
/*
BOOST_AUTO_TEST_CASE(testPlayUnplay){
	bool trickend, friendwin;

	fakeGenerateHands(_state);

	_state->print();

	_state->play(1, trickend, friendwin);//7coeur
	_state->print();
	_state->play(3, trickend, friendwin);//valetcoeur
	BOOST_CHECK_EQUAL(trickend, true);
	BOOST_CHECK_EQUAL(friendwin, true);
	_state->print();
	_state->play(0, trickend, friendwin);
	_state->print();
	_state->unplay();
	_state->print();
	_state->unplay();
	_state->print();
}
/*
BOOST_AUTO_TEST_CASE(unPlay){
	BOOST_FAIL("not implemented");

}*/


BOOST_AUTO_TEST_SUITE_END()





BOOST_FIXTURE_TEST_SUITE(minmaxAlphaBeta, SetUpGameState)
BOOST_AUTO_TEST_CASE(testIA){
	fakeGenerateHands(_state);

	_state->print();
	freopen("print_tree.txt","w",stdout);

	AlphaBeta ia;
	ia.minMaxAlphaBeta(*_state, 3);

}

BOOST_AUTO_TEST_CASE(testAdvancedAuthorized){
	bool trickEnd, friendWin;
	fakeGenerateHands(_state);
	vector<int> vecAuthorized;
	_state->authorized(vecAuthorized);

	BOOST_CHECK_EQUAL(vecAuthorized.size(), 2);
	vecAuthorized.clear();

	_state->play(1,trickEnd, friendWin);
	_state->authorized(vecAuthorized);
	BOOST_CHECK_EQUAL(vecAuthorized.size(), 1);

	vecAuthorized.clear();
	_state->play(3,trickEnd, friendWin);
	_state->authorized(vecAuthorized);
	BOOST_CHECK_EQUAL(vecAuthorized.size(), 7);

	vecAuthorized.clear();
	_state->play(0,trickEnd, friendWin);
	_state->authorized(vecAuthorized);
	BOOST_CHECK_EQUAL(vecAuthorized.size(), 7);

	vecAuthorized.clear();
	_state->play(0,trickEnd, friendWin);
	_state->authorized(vecAuthorized);
	BOOST_CHECK_EQUAL(vecAuthorized.size(), 1);

	vecAuthorized.clear();
	_state->play(0,trickEnd, friendWin);
	_state->authorized(vecAuthorized);
	BOOST_CHECK_EQUAL(vecAuthorized.size(), 3);

}
BOOST_AUTO_TEST_SUITE_END()
